# 项目源码地址
### &emsp;https://gitee.com/hl0109/vue-split-drag-pane
### &emsp;一个可拖动调整面板布局大小的vue组件，支持vue2与vue3，支持PC与移动端操作

## 演示 
### &emsp;npm社区打不开时参考 https://gitee.com/hl0109/vue-split-drag-pane

<img src="public/al.gif">

## 安装
```shell
npm install vue-split-drag-pane -S
```

## 案例
```html
<template>
    <div style="width:100vw;height: 100vh;">
        <split-drag-pane  @change="change" :span="10">
            <split-drag-pane default-percent="100px" vmid="1" @change="change">
                <div style="width: 100%;height: 100%;background-color: aqua;">
                    1
                </div>
            </split-drag-pane>
            <split-drag-pane  vmid="2" default-percent="calc(70% - 100px)" @change="change" :span="20">
                <div style="width: 100%;height: 100%;background-color: bisque;">
                    2
                </div>
            </split-drag-pane>
            <split-drag-pane default-percent="30" vmid="3" split="horizontal" @change="change">
                <split-drag-pane default-percent="30" vmid="3-1" @change="change">
                    <div style="width: 100%;height: 100%;background-color: #07a1f3;">
                        3-1
                    </div>
                </split-drag-pane>
                <split-drag-pane default-percent="40" vmid="3-2" @change="change">
                    <div style="width: 100%;height: 100%;background-color: #9cf3a9;">
                        3-2
                    </div>
                </split-drag-pane>
                <split-drag-pane default-percent="30" vmid="3-3" @change="change">
                    <div style="width: 100%;height: 100%;background-color: #f3e0a7;">
                        3-3
                    </div>
                </split-drag-pane>
            </split-drag-pane>
        </split-drag-pane>
    </div>
</template>
<script>
import {splitDragPane} from 'vue-split-drag-pane'
export default {
    components: {splitDragPane},
    methods: {
        change:function(val,vmid) {
            console.log("val,vmid",val,vmid
            )
        }
    }
}
</script>
```


## 说明
###  &emsp;1.属性(Attributes)
<table style="width: 100%">
<tr><td>参数</td><td>说明</td><td>类型</td><td>可选值</td><td>默认值</td></tr>
<tr><td>split</td><td>vertical:左右排列; horizontal:上下排列</td><td>string</td><td>vertical / horizontal</td><td>vertical</td></tr>
<tr><td>minPercent</td><td>最小宽度或者高度,支持像素与百分比，举例值可以是10px、10(默认百分比)</td><td>string / number</td><td>-</td><td>50px</td></tr>
<tr><td>defaultPercent</td><td>默认宽度或者高度，举例值可以10px、10(默认百分比)</td><td>number</td><td>-</td><td>-</td></tr>
<tr><td>vmid</td><td>dom虚拟id标识</td><td>string</td><td>-</td><td>-</td></tr>
<tr><td>span</td><td>排列布局直接的间隔，单位px，举例值10px</td><td>number</td><td>-</td><td>0</td></tr>
</table>

###  &emsp;2.事件(Events)
<table style="width: 100%">
<tr><td>事件名称</td><td>说明</td><td>参数</td></tr>
<tr><td>change</td><td>percent:拖到后的占比; vmid:自定义的dom虚拟id标识</td><td>percent,vmid</td></tr>
</table>
