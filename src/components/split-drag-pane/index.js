import splitDragPane from './index.vue';


splitDragPane.install = function(Vue) {
  Vue.component(splitDragPane.name, splitDragPane);
};

export function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Vue.component(splitDragPane.name, splitDragPane);
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
} else if (typeof global !== 'undefined' && window.Vue) {
  install(window.Vue);
}
//导出列表
export {
  splitDragPane
};
//默认导出
export default {
  version: '0.1.2',
  install,
  compontents:{splitDragPane}
};